package com.reader.rfid.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity
public class ProductTag {
    @PrimaryKey(autoGenerate = true)
    public int uid;
    @ColumnInfo(name = "tag")
    public String tag;
    @ColumnInfo(name = "product")
    public String product;

    /**
     * Default Constructor
     * <p>
     * Room Database will use this no-arg constructor by default.
     * The others are annotated with @Ignore,
     * so Room will not give a warning about "Multiple Good Constructors".
     */
    public ProductTag() {
    }

    @Ignore
    public ProductTag(String tag) {
        this.tag = tag;
    }
    @Ignore
    public ProductTag(String tag, String product) {
        this.tag = tag;
        this.product = product;
    }

    public String getTag() {
        return tag;
    }

    public void setTagID(String tag) {
        this.tag = tag;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }
}
