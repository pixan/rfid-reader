package com.reader.rfid.models.response;

import com.reader.rfid.models.ProductTag;

public class ResponseProductTag {
    private ProductTag[] tags;

    public ResponseProductTag(ProductTag[] tags) {
        this.tags = tags;
    }

    public ProductTag[] getTags() {
        return tags;
    }

    public void setTags(ProductTag[] tags) {
        this.tags = tags;
    }
}
