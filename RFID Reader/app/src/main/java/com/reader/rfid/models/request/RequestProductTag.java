package com.reader.rfid.models.request;

import java.util.ArrayList;

public class RequestProductTag {
    final ArrayList<Tag> tags;

    public RequestProductTag(ArrayList<Tag> tags) {
        this.tags = tags;
    }
}
