package com.reader.rfid.interfaces;

import com.reader.rfid.models.APIError;
import com.reader.rfid.models.response.ResponseBody;

public interface ResponseCallback {
    void baseResponse(ResponseBody responseBody);
    void baseError(APIError loginError);
}
