package com.reader.rfid.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.reader.rfid.models.warehouse.Warehouse;

import java.util.List;

@Dao
public interface WarehouseDAO {
    @Query("SELECT * FROM Warehouse")
    LiveData<List<Warehouse>> getAll();

    @Query("SELECT name FROM Warehouse")
    LiveData<List<String>> getAllNames();

    @Insert
    void insertWarehouse(Warehouse warehouse);

    @Query("DELETE FROM Warehouse")
    void deleteAll();

    @Query("SELECT * FROM Warehouse WHERE name = :getName")
    Warehouse getWarehouseByName(String getName);
}
