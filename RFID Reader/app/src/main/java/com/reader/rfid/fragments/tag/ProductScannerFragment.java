package com.reader.rfid.fragments.tag;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputEditText;
import com.honeywell.rfidservice.EventListener;
import com.honeywell.rfidservice.RfidManager;
import com.honeywell.rfidservice.TriggerMode;
import com.honeywell.rfidservice.rfid.RfidReader;
import com.reader.rfid.R;
import com.reader.rfid.activities.App;
import com.reader.rfid.activities.BluetoothActivity;
import com.reader.rfid.activities.ReadActivity;
import com.reader.rfid.databinding.FragmentProductScannerBinding;
import com.reader.rfid.models.ProductTag;
import com.reader.rfid.recyclerview.ProductTagAdapter;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class ProductScannerFragment extends Fragment {

    private ProductScannerViewModel viewModel;
    private FragmentProductScannerBinding binding;

    //View elements
    private TextInputEditText editTextScanner;
    private TextView tagCounter;
    private ImageView rfidStatus;

    // MyApp
    private App myApp;

    // RFID
    private RfidManager mRfidMgr;
    private String macAddress;

    // Handlers and dialogs
    private ProgressDialog mWaitDialog;
    private Handler mHandler = new Handler();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = FragmentProductScannerBinding.inflate(inflater, container, false);
        editTextScanner = binding.editTextScanner;
        tagCounter = binding.tagCounter;
        binding.rfidConnectCircle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getRfidStatus()) {
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mRfidMgr.createReader();
                        }
                    }, 1000);
                    mWaitDialog = ProgressDialog
                            .show(getActivity(), getResources().getString(R.string.connecting), getResources().getString(R.string.creaetingReader));
                } else {
                    Intent intent = new Intent(getActivity(), BluetoothActivity.class);
                    startActivity(intent);
                }
            }
        });
        rfidStatus = (ImageView) binding.rfidStatusCircle;
        setHasOptionsMenu(true);

        myApp = App.getInstance();
        mRfidMgr = myApp.rfidManager;
        macAddress = myApp.getMacAddress();
        setRfidStatus();
        return binding.getRoot();
    }

    @Override
    public void onCreateOptionsMenu(@NonNull @NotNull Menu menu, @NonNull @NotNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull @NotNull MenuItem item) {
        if (item.getItemId() == R.id.delete_all_products) {
            viewModel.deleteAll();
            return true;
        }
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        setRfidStatus();
        mRfidMgr.addEventListener(mEventListener);
    }

    @Override
    public void onPause() {
        super.onPause();
        mRfidMgr.removeEventListener(mEventListener);
    }

    private void setRfidStatus() {
        if (getRfidStatus()) {
            rfidStatus.setImageDrawable(getResources().getDrawable(R.drawable.shape_circle_status_green));
        } else {
            rfidStatus.setImageDrawable(getResources().getDrawable(R.drawable.shape_circle_status_red));
        }
    }

    private boolean getRfidStatus() {
        return macAddress != null && !macAddress.isEmpty() && mRfidMgr != null && mRfidMgr.isConnected();
    }

    private void closeWaitDialog() {
        if (mWaitDialog != null) {
            mWaitDialog.dismiss();
            mWaitDialog = null;
        }
    }


    private EventListener mEventListener = new EventListener() {
        @Override
        public void onDeviceConnected(Object o) {
            myApp.macAddress = (String) o;

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    setRfidStatus();
                    closeWaitDialog();
                }
            });
        }

        @Override
        public void onDeviceDisconnected(Object o) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    setRfidStatus();
                    closeWaitDialog();
                }
            });
        }

        @Override
        public void onReaderCreated(boolean b, RfidReader rfidReader) {
            myApp.getInstance().mRfidManager = rfidReader;
            closeWaitDialog();
            Intent intent = new Intent(getActivity(), ReadActivity.class);
            startActivity(intent);
        }

        @Override
        public void onRfidTriggered(boolean b) {
        }

        @Override
        public void onTriggerModeSwitched(TriggerMode triggerMode) {
        }
    };

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView recyclerView = getActivity().findViewById(R.id.recycle_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setHasFixedSize(true);

        ProductTagAdapter adapter = new ProductTagAdapter();
        recyclerView.setAdapter(adapter);

        viewModel = new ViewModelProvider(this).get(ProductScannerViewModel.class);
        // Populate tags
        viewModel.getAllTags().observe(getViewLifecycleOwner(), new Observer<List<ProductTag>>() {
            @Override
            public void onChanged(@Nullable List<ProductTag> productTags) {
                adapter.setProductTags(productTags);
                int numCount = adapter.getItemCount();
                if (numCount > 0) {
                    tagCounter.setVisibility(TextView.VISIBLE);
                    tagCounter.setText(getResources().getString(R.string.count) + String.valueOf(numCount));
                } else {
                    tagCounter.setVisibility(TextView.GONE);
                }
            }
        });
        // Handle keyboard behavior
        editTextScanner.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                String text = editTextScanner.getText().toString();
                if (actionId == EditorInfo.IME_ACTION_DONE && !text.isEmpty()) {
                    ProductTag productTag = new ProductTag(text);
                    viewModel.insert(productTag);
                    editTextScanner.setText("");
                    editTextScanner.clearFocus();
                }
                return false;
            }
        });

        editTextScanner.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 10) {
                    String barcode = s.toString();
                    editTextScanner.setText("");
                    ProductTag productTag = new ProductTag(barcode);
                    viewModel.insert(productTag);

                }

            }
        });

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull @NotNull RecyclerView recyclerView, @NonNull @NotNull RecyclerView.ViewHolder viewHolder, @NonNull @NotNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull @NotNull RecyclerView.ViewHolder viewHolder, int direction) {
                viewModel.delete(adapter.getProductTagAt(viewHolder.getAdapterPosition()));
                Toast.makeText(getContext(), getResources().getString(R.string.tagRemovedAlert), Toast.LENGTH_SHORT).show();
            }
        }).attachToRecyclerView(recyclerView);

        adapter.setOnRemoveClickListener(new ProductTagAdapter.OnRemoveClickListener() {
            @Override
            public void onItemClick(ProductTag productTag) {
                viewModel.delete(productTag);
            }
        });
    }
}
