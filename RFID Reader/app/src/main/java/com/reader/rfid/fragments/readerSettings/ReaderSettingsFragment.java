package com.reader.rfid.fragments.readerSettings;

import android.os.Bundle;

import androidx.preference.PreferenceFragmentCompat;

import com.reader.rfid.R;

public class ReaderSettingsFragment extends PreferenceFragmentCompat {

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.fragment_settings_reader, rootKey);
    }
}
