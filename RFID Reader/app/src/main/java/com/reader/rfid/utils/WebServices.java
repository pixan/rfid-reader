package com.reader.rfid.utils;

import android.content.Context;

import com.reader.rfid.R;
import com.reader.rfid.interfaces.ResponseCallback;
import com.reader.rfid.interfaces.ResponseProductTagCallback;
import com.reader.rfid.models.APIError;
import com.reader.rfid.models.ProductTag;
import com.reader.rfid.models.login.LoginRequest;
import com.reader.rfid.models.request.RequestProductTag;
import com.reader.rfid.models.request.StockReviewRequest;
import com.reader.rfid.models.response.ResponseBody;
import com.reader.rfid.models.response.ResponseProductTag;
import com.reader.rfid.rest.ApiRestAdapter;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WebServices {
    public static void wsPostLogin(LoginRequest loginRequest, Context context,
            ResponseCallback callback) {
        Call<ResponseBody> loginRequestCall;
        loginRequestCall = new ApiRestAdapter().getApiRest().wsPostLogin(loginRequest);
        Callback<ResponseBody> baseCallback = new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    callback.baseResponse(response.body());
                } else {
                    APIError error = new ErrorUtils().getLoginErrors(response, context);
                    callback.baseError(error);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                callback.baseError(new APIError(0, context.getString(R.string.requestFailed)));
            }
        };
        loginRequestCall.enqueue(baseCallback);
    }

    public static void wsPostTags(RequestProductTag request, Context context, ResponseProductTagCallback callback) {
        Call<List<ProductTag>> productTagCall;
        productTagCall = new ApiRestAdapter().getApiRest().wsPostTags(request);
        Callback<List<ProductTag>> baseCallback = new Callback<List<ProductTag>>() {
            @Override
            public void onResponse(Call<List<ProductTag>> call, Response<List<ProductTag>> response) {
                if (response.isSuccessful()) {
                    List<ProductTag> productTags = response.body();
                    callback.baseResponse(productTags);
                }
            }

            @Override
            public void onFailure(Call<List<ProductTag>> call, Throwable t) {
                callback.baseError(new APIError(0, context.getString(R.string.requestFailed)));
            }

        };
        productTagCall.enqueue(baseCallback);
    }


    public static void wsStockReview(StockReviewRequest request, Context context, ResponseCallback callback) {
        Call<ResponseBody> responseStockReviewCall;
        responseStockReviewCall = new ApiRestAdapter().getApiRest().wsPostStockReview(request);
        Callback<ResponseBody> baseCallback = new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    callback.baseResponse(response.body());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                callback.baseError(new APIError(0, context.getString(R.string.requestFailed)));
            }

        };
        responseStockReviewCall.enqueue(baseCallback);
    }
}
