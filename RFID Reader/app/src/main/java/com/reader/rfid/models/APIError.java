package com.reader.rfid.models;

public class APIError {
    private final int statusCode;
    private final String message;

    public APIError(int statusCode, String message) {
        this.statusCode = statusCode;
        this.message = message;
    }

    public int status() {
        return statusCode;
    }

    public String message() {
        return message;
    }
}
