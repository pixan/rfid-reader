package com.reader.rfid.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.reader.rfid.R;
import com.reader.rfid.models.warehouse.Warehouse;

import java.util.ArrayList;
import java.util.List;

public class WarehouseMenuAdapter extends ArrayAdapter<Warehouse> {
    private Context mContext;
    private List<Warehouse> wareHouseList = new ArrayList<>();

    public WarehouseMenuAdapter(Context context, List<Warehouse> list) {
        super(context, 0 , list);
        mContext = context;
        wareHouseList = list;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;

        if(listItem == null)
            listItem = LayoutInflater.from(mContext).inflate(R.layout.item_dropdown_warehouse,parent,false);

        Warehouse currentWarehouse = wareHouseList.get(position);

        TextView textView = (TextView) listItem.findViewById(R.id.item_dropdown_textView);
        textView.setText(currentWarehouse.getName());

        return listItem;

    }

    @Nullable
    @Override
    public Warehouse getItem(int position) {
        return wareHouseList.get(position);
    }
}
