package com.reader.rfid.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;

import com.honeywell.aidc.BarcodeFailureEvent;
import com.honeywell.aidc.BarcodeReadEvent;
import com.honeywell.aidc.BarcodeReader;
import com.honeywell.aidc.ScannerUnavailableException;
import com.honeywell.aidc.TriggerStateChangeEvent;
import com.honeywell.aidc.UnsupportedPropertyException;
import com.reader.rfid.R;
import com.reader.rfid.adapters.WarehouseMenuAdapter;
import com.reader.rfid.fragments.tag.ProductScannerViewModel;
import com.reader.rfid.interfaces.ResponseCallback;
import com.reader.rfid.models.APIError;
import com.reader.rfid.models.ProductTag;
import com.reader.rfid.models.request.StockReviewRequest;
import com.reader.rfid.models.response.ResponseBody;
import com.reader.rfid.models.warehouse.Warehouse;
import com.reader.rfid.utils.WebServices;
import com.reader.rfid.viewModel.WarehouseViewModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class InventaryActivity extends AppCompatActivity implements BarcodeReader.BarcodeListener, BarcodeReader.TriggerListener {

    Toolbar toolbar;

    private Button saveButton;
    private AutoCompleteTextView autoCompleteTextView;
    private LinearLayout productTagScannerLayout;

    private WarehouseViewModel warehouseViewModel;
    private ProductScannerViewModel productScannerViewModel;
    private com.honeywell.aidc.BarcodeReader barcodeReader;

    private Warehouse warehouseSelected;

    // Handlers and dialogs
    private ProgressDialog mWaitDialog;

    private WarehouseMenuAdapter arrayAdapter;

    List<ProductTag> productTags;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventary);
        toolbar = findViewById(R.id.toolbar);
        productTagScannerLayout = (LinearLayout) findViewById(R.id.productTagScannerLayout);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        toolbar.setTitle(R.string.menuItemInventarySurvey);

        barcodeReader = HomeActivity.getBarcodeObject();

        if (barcodeReader != null) {
            // register bar code event listener
            barcodeReader.addBarcodeListener(this);
            // set the trigger mode to client control
            try {
                barcodeReader.setProperty(BarcodeReader.PROPERTY_TRIGGER_CONTROL_MODE,
                        BarcodeReader.TRIGGER_CONTROL_MODE_AUTO_CONTROL);
            } catch (UnsupportedPropertyException e) {
                Toast
                        .makeText(this, "Failed to apply properties", Toast.LENGTH_SHORT)
                        .show();
            }
            // register trigger state change listener
            barcodeReader.addTriggerListener(this);
            Map<String, Object> properties = new HashMap<String, Object>();
            // Set Symbologies On/Off
            properties.put(BarcodeReader.PROPERTY_CODE_128_ENABLED, true);
            properties.put(BarcodeReader.PROPERTY_GS1_128_ENABLED, true);
            properties.put(BarcodeReader.PROPERTY_QR_CODE_ENABLED, true);
            properties.put(BarcodeReader.PROPERTY_CODE_39_ENABLED, true);
            properties.put(BarcodeReader.PROPERTY_DATAMATRIX_ENABLED, true);
            properties.put(BarcodeReader.PROPERTY_UPC_A_ENABLE, true);
            properties.put(BarcodeReader.PROPERTY_EAN_13_ENABLED, false);
            properties.put(BarcodeReader.PROPERTY_AZTEC_ENABLED, false);
            properties.put(BarcodeReader.PROPERTY_CODABAR_ENABLED, false);
            properties.put(BarcodeReader.PROPERTY_INTERLEAVED_25_ENABLED, false);
            properties.put(BarcodeReader.PROPERTY_PDF_417_ENABLED, false);
            // Set Max Code 39 barcode length
            properties.put(BarcodeReader.PROPERTY_CODE_39_MAXIMUM_LENGTH, 10);
            // Turn on center decoding
            properties.put(BarcodeReader.PROPERTY_CENTER_DECODE, true);
            // Enable bad read response
            properties.put(BarcodeReader.PROPERTY_NOTIFICATION_BAD_READ_ENABLED, true);
            // Apply the settings
            barcodeReader.setProperties(properties);
        }

        warehouseViewModel = new ViewModelProvider(this).get(WarehouseViewModel.class);
        autoCompleteTextView = (AutoCompleteTextView)findViewById(R.id.warehouseSelect);

        autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                warehouseSelected = arrayAdapter.getItem(position);
                assert warehouseSelected != null;
                autoCompleteTextView.setText(warehouseSelected.getName());
                assert productTagScannerLayout != null;
                productTagScannerLayout.setVisibility(View.VISIBLE);

            }
        });

        productScannerViewModel = new ViewModelProvider(this)
                .get(ProductScannerViewModel.class);

    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        // Populate warehouses
        warehouseViewModel.getAllWarehouses().observe(this, warehouses -> {
            arrayAdapter = new WarehouseMenuAdapter(this, warehouses);
            autoCompleteTextView.setAdapter(arrayAdapter);
        });
        productScannerViewModel.getAllTags().observe(this, tags -> {
            productTags = tags;
        });
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        productScannerViewModel.deleteAll();
        super.onDestroy();
    }

    @Override
    public void onBarcodeEvent(BarcodeReadEvent barcodeReadEvent) {
        runOnUiThread(() -> {
            ProductTag productTag = new ProductTag(barcodeReadEvent.getBarcodeData());
            productScannerViewModel.insert(productTag);
        });

    }
    @Override
    public void onFailureEvent(BarcodeFailureEvent barcodeFailureEvent) { }
    @Override
    public void onTriggerEvent(TriggerStateChangeEvent triggerStateChangeEvent) { }
    @Override
    public void onResume() {
        super.onResume();
        if (barcodeReader != null) {
            try {
                barcodeReader.claim();
            } catch (ScannerUnavailableException e) {
                e.printStackTrace();
                Toast.makeText(this, "Scanner unavailable", Toast.LENGTH_SHORT).show();
            }
        }
    }
    public void onSaveInventary(View view) {
        if (warehouseSelected == null) {
            Toast.makeText(this, "Debe seleccionar un almacen primero", Toast.LENGTH_SHORT).show();
        } else if(productTags.size() == 0) {
            Toast.makeText(this, "Productos no escaneados", Toast.LENGTH_SHORT).show();
        } else {
            try {
                ArrayList<String> serialNumber = new ArrayList<>();
                for (int i = 0; i < productTags.size(); i++) {
                    serialNumber.add(productTags.get(i).tag);
                }
                if(!serialNumber.isEmpty()) {
                    mWaitDialog = ProgressDialog.show(this, null, "Cargando productos ...");
                    mWaitDialog.setCancelable(false);
                    StockReviewRequest stockReviewRequest = new StockReviewRequest(warehouseSelected.getId(), serialNumber);
                    WebServices.wsStockReview(stockReviewRequest, this, new ResponseCallback() {
                        @Override
                        public void baseResponse(ResponseBody responseBody) {
                            System.out.println(responseBody);
                            closeWaitDialog();
                            warehouseSelected = null;
                            autoCompleteTextView.setText(null);
                            productScannerViewModel.deleteAll();
                            productTagScannerLayout.setVisibility(View.GONE);
                            AlertDialog.Builder builder = new AlertDialog.Builder(InventaryActivity.this);
                            builder.setMessage("Productos ingresados al sistema").setTitle("Listo");
                            builder.setNegativeButton(R.string.close, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });
                            AlertDialog dialog = builder.create();
                            dialog.show();
                        }

                        @Override
                        public void baseError(APIError loginError) {
                            closeWaitDialog();
                            AlertDialog.Builder builder = new AlertDialog.Builder(InventaryActivity.this);
                            builder.setMessage(loginError.message().toString()).setTitle("Error: " + String.valueOf(loginError.status()));
                            builder.setNegativeButton(R.string.close, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });
                            AlertDialog dialog = builder.create();
                            dialog.show();
                        }
                    });
                }
            } catch (Exception e) {
                System.out.println(e);
            }


        }
    }
    private void closeWaitDialog() {
        if (mWaitDialog != null) {
            mWaitDialog.dismiss();
            mWaitDialog = null;
        }
    }
}