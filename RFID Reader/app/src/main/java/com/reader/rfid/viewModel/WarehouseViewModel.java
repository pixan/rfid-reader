package com.reader.rfid.viewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.reader.rfid.models.ProductTag;
import com.reader.rfid.models.warehouse.Warehouse;
import com.reader.rfid.repository.WarehouseRepository;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class WarehouseViewModel extends AndroidViewModel {
    private WarehouseRepository repository;
    private LiveData<List<Warehouse>> allWarehouses;
    private LiveData<List<String>> allWarehousesNames;

    public WarehouseViewModel(@NonNull @NotNull Application application) {
        super(application);
        repository = new WarehouseRepository(application);
        allWarehouses = repository.getAllWarehouse();
        allWarehousesNames = repository.getAllWarehouseNames();
    }
    public LiveData<List<Warehouse>> getAllWarehouses() {
        return allWarehouses;
    }
    public LiveData<List<String>> getAllWarehousesNames() {
        return allWarehousesNames;
    }
    public void insert(Warehouse warehouse) {
        repository.insertList(warehouse);
    }
    public Warehouse getWareHouseByName(String name) {
        return repository.getWarehouseByName(name);
    }
}
