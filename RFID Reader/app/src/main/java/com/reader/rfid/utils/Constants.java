package com.reader.rfid.utils;

public class Constants {
    // Todo: Change the real endpoint that we'll use to this project
    public static final String BASE_URL = "https://wms.bwdigital.io/api/";

    // Login
    // Todo: Change endpoints
    public static final String LOGIN = "auth/login";

    // Tags
    public static final String GET_TAGS = "tags";

    // Stock
    public static final String STOCK_REVIEW = "stock-review";
}
