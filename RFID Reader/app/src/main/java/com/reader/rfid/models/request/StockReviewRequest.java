package com.reader.rfid.models.request;

import java.util.ArrayList;

public class StockReviewRequest {
    final int warehouse_id;
    final ArrayList<String> serial_numbers;


    public StockReviewRequest(int warehouse_id, ArrayList<String> serial_numbers) {
        this.warehouse_id = warehouse_id;
        this.serial_numbers = serial_numbers;
    }
}
