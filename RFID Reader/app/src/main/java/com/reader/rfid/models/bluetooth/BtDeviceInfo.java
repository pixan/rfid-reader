package com.reader.rfid.models.bluetooth;

import android.bluetooth.BluetoothDevice;

public class BtDeviceInfo {
    public BluetoothDevice dev;
    public int rssi;

    public BtDeviceInfo(BluetoothDevice dev, int rssi) {
        this.dev = dev;
        this.rssi = rssi;
    }

}
