package com.reader.rfid.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.reader.rfid.models.ProductTag;

import java.util.List;

@Dao
public interface ProductTagDAO {
    @Query("SELECT * FROM ProductTag")
    LiveData<List<ProductTag> > getAll();

    @Insert
    void insert(ProductTag productTag);

    @Delete
    void delete(ProductTag productTag);

    @Query("DELETE FROM ProductTag")
    void deleteAll();
}
