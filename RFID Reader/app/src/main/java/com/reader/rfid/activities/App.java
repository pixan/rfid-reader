package com.reader.rfid.activities;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.SystemClock;

import androidx.preference.PreferenceManager;

import com.honeywell.rfidservice.RfidManager;
import com.honeywell.rfidservice.rfid.RfidReader;
import com.reader.rfid.R;

public class App extends Application {

    private static App mInstance;
    // RFID
    public RfidManager rfidManager;
    public RfidReader mRfidManager;
    public String macAddress;

    // Shared pref
    public String macAddPref = "MACADDRESSPREF";
    public String macAddFlag = "MACADDRESSFLAG";



    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;
        rfidManager = RfidManager.getInstance(this);

        PreferenceManager.setDefaultValues(this, R.xml.fragment_settings_reader, false);

        // MACADDRESS
        SharedPreferences macAddStore = getSharedPreferences(macAddPref, Context.MODE_PRIVATE);
        macAddress = macAddStore.getString(macAddFlag, "");
        SystemClock.sleep(4000);
    }

    public static App getInstance() {
        return mInstance;
    }

    public String getMacAddress() { return macAddress; }

    public RfidReader getmRfidReader() { return mRfidManager; }

    public void setMacAddress(String macAddress) {
        SharedPreferences preferencias=getSharedPreferences(macAddPref,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=preferencias.edit();
        editor.putString(macAddFlag, macAddress);
        editor.commit();
        this.macAddress = macAddress;
    }
}
