package com.reader.rfid.repository;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.reader.rfid.db.AppDatabase;
import com.reader.rfid.db.dao.WarehouseDAO;
import com.reader.rfid.models.warehouse.Warehouse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class WarehouseRepository {
    private WarehouseDAO wareHouseDAO;
    private LiveData<List<Warehouse>> allWarehouse;
    private LiveData<List<String>> allWarehouseNames;

    public WarehouseRepository(Application application) {
        AppDatabase db  = AppDatabase.getInstance((application));
        wareHouseDAO = db.warehouseDao();
        allWarehouse = wareHouseDAO.getAll();
        allWarehouseNames = wareHouseDAO.getAllNames();
    }
    public void insertList (Warehouse warehouse) {
        new InsertWarehouseAsyncTask(wareHouseDAO).execute(warehouse);
    }
    public LiveData<List<Warehouse>> getAllWarehouse() { return allWarehouse; }
    public LiveData<List<String>> getAllWarehouseNames() { return allWarehouseNames; }
    public Warehouse getWarehouseByName(String name) {
        return wareHouseDAO.getWarehouseByName(name);
    }

    // Tasks
    private static class InsertWarehouseAsyncTask extends AsyncTask<Warehouse, Void, Void> {
        private WarehouseDAO wareHouseDAO;

        public InsertWarehouseAsyncTask(WarehouseDAO wareHouseDAO) {
            this.wareHouseDAO = wareHouseDAO;
        }

        @Override
        protected Void doInBackground(Warehouse... warehouses) {
            wareHouseDAO.insertWarehouse(warehouses[0]);
            return null;
        }
    }
 }
