package com.reader.rfid.fragments.tag;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.reader.rfid.models.ProductTag;
import com.reader.rfid.repository.ProductTagRepository;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Objects;

public class ProductScannerViewModel extends AndroidViewModel {
    private ProductTagRepository repository;
    private LiveData<List<ProductTag>> allTags;

    public ProductScannerViewModel(@NonNull @NotNull Application application) {
        super(application);
        repository = new ProductTagRepository(application);
        allTags = repository.getAllTags();
    }
    public int getSize() { return Objects.requireNonNull(allTags.getValue()).size(); }
    public void insert(ProductTag productTag) {
        repository.insert(productTag);
    }
    public void delete(ProductTag productTag) {
        repository.delete(productTag);
    }
    public void deleteAll() {
        repository.deleteAll();
    }
    public LiveData<List<ProductTag>> getAllTags() {
        return allTags;
    }
}
