package com.reader.rfid.rest;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

// Responses
import com.reader.rfid.models.ProductTag;
import com.reader.rfid.models.login.LoginRequest;
import com.reader.rfid.models.request.RequestProductTag;
import com.reader.rfid.models.request.StockReviewRequest;
import com.reader.rfid.models.response.ResponseBody;
import com.reader.rfid.models.response.ResponseProductTag;

import static com.reader.rfid.utils.Constants.LOGIN;
import static com.reader.rfid.utils.Constants.GET_TAGS;
import static com.reader.rfid.utils.Constants.STOCK_REVIEW;

import java.util.List;


public interface ApiRestTasks {
    @POST(LOGIN)
    Call<ResponseBody> wsPostLogin(@Body LoginRequest body);

    @POST(GET_TAGS)
    Call<List<ProductTag>> wsPostTags(@Body RequestProductTag body);

    @POST(STOCK_REVIEW)
    Call<ResponseBody> wsPostStockReview(@Body StockReviewRequest body);

}
