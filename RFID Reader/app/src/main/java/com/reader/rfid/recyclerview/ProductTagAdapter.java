package com.reader.rfid.recyclerview;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.reader.rfid.R;
import com.reader.rfid.models.ProductTag;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;


public class ProductTagAdapter extends RecyclerView.Adapter<ProductTagAdapter.ProductTagHolder> {

    private List<ProductTag> tags = new ArrayList<>();
    private OnRemoveClickListener listener;

    @Override
    public ProductTagHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_product_tag, parent, false);
        return new ProductTagHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NotNull ProductTagHolder holder, int position) {
        ProductTag product = tags.get(position);
        holder.titleTag.setText(product.getProduct() == null ? product.getTag() : product.getProduct());
        holder.descTag.setText(product.getTag());
    }

    @Override
    public int getItemCount() {
        return tags.size();
    }

    public void setProductTags(List<ProductTag> tags) {
        this.tags = tags;
        notifyDataSetChanged();
    }

    public ProductTag getProductTagAt(int position) {
        return tags.get(position);
    }

    class ProductTagHolder extends RecyclerView.ViewHolder {
        private TextView titleTag;
        private TextView descTag;
        private ImageButton removeButton;

        public ProductTagHolder(View itemView) {
            super(itemView);
            titleTag = itemView.findViewById(R.id.titleTag);
            descTag = itemView.findViewById(R.id.descTag);
            removeButton = itemView.findViewById(R.id.removeButton);
            removeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (listener != null && position != RecyclerView.NO_POSITION) {
                        listener.onItemClick(tags.get(position));
                    }
                }
            });
        }
    }

    public interface OnRemoveClickListener {
        void onItemClick(ProductTag productTag);
    }

    public void setOnRemoveClickListener(OnRemoveClickListener listener) {
        this.listener = listener;
    }

}
