package com.reader.rfid.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.honeywell.rfidservice.EventListener;
import com.honeywell.rfidservice.RfidManager;
import com.honeywell.rfidservice.TriggerMode;
import com.honeywell.rfidservice.rfid.OnTagReadListener;
import com.honeywell.rfidservice.rfid.RfidReader;
import com.honeywell.rfidservice.rfid.TagAdditionData;
import com.honeywell.rfidservice.rfid.TagReadData;
import com.honeywell.rfidservice.rfid.TagReadOption;
import com.reader.rfid.R;
import com.reader.rfid.fragments.tag.ProductScannerViewModel;
import com.reader.rfid.interfaces.ResponseProductTagCallback;
import com.reader.rfid.models.APIError;
import com.reader.rfid.models.ProductTag;
import com.reader.rfid.models.request.RequestProductTag;
import com.reader.rfid.models.request.Tag;
import com.reader.rfid.models.response.ResponseProductTag;
import com.reader.rfid.utils.WebServices;

import java.util.ArrayList;
import java.util.List;

public class ReadActivity extends AppCompatActivity {

    Toolbar toolbar;

    // RFID
    private RfidManager mRfidMgr;
    private RfidReader mReader;
    private List mTagDataList = new ArrayList();

    // View
    private boolean mIsReadBtnClicked;
    private Button mBtnRead;
    private Button mBtnSave;
    private ListView mLv;
    private ArrayAdapter mAdapter;

    // Handlers and dialogs
    private ProgressDialog mWaitDialog;

    // View model
    private ProductScannerViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        // RFID
        mRfidMgr = App.getInstance().rfidManager;
        mReader = App.getInstance().getmRfidReader();
        setContentView(R.layout.activity_read);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        toolbar.setTitle(R.string.activityReadTitle);
        toolbar.setSubtitle(R.string.activityReadSub);
        // VIEW
        mBtnRead = findViewById(R.id.btn_read);
        mBtnSave = findViewById(R.id.btn_save);
        mLv = findViewById(R.id.lv);
        mAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_activated_1, mTagDataList);
        mLv.setAdapter(mAdapter);
        viewModel = new ViewModelProvider(this).get(ProductScannerViewModel.class);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.tagid_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.readerSettings) {
            startActivity(new Intent(this, ReaderSettingsActivity.class));
            return true;
        } else if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mRfidMgr.addEventListener(mEventListener);
        showBtn();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mRfidMgr.removeEventListener(mEventListener);
        mIsReadBtnClicked = false;
        stopRead();
    }

    private void showBtn() {
        if (mIsReadBtnClicked) {
            mBtnRead.setText(R.string.stop);
            mBtnRead.setTextColor(Color.rgb(255, 128, 0));
            mBtnSave.setEnabled(false);
            mBtnSave.setBackgroundColor(getResources().getColor(R.color.baseRedStatus));
            mBtnSave.getBackground().setAlpha(64);
        } else {
            mBtnRead.setText((R.string.read));
            mBtnRead.setTextColor(Color.rgb(255, 255, 255));
            if (!mAdapter.isEmpty() && mAdapter.getCount() > 0) {
                mBtnSave.setEnabled(true);
                mBtnSave.getBackground().setAlpha(255);
                mBtnSave.setBackgroundColor(getResources().getColor(R.color.baseGreenStatus));
            } else {
                mBtnSave.setEnabled(false);
                mBtnSave.getBackground().setAlpha(64);
                mBtnSave.setBackgroundColor(getResources().getColor(R.color.baseRedStatus));
            }
        }
    }

    public void clickBtnRead(View view) {
        if (mIsReadBtnClicked) {
            mIsReadBtnClicked = false;
            stopRead();
        } else {
            mIsReadBtnClicked = true;
            read();
        }
        showBtn();
    }

    private void closeWaitDialog() {
        if (mWaitDialog != null) {
            mWaitDialog.dismiss();
            mWaitDialog = null;
        }
    }

    public void clickBtnSave(View view) {
        showBtn();
        if (!mAdapter.isEmpty() && mAdapter.getCount() > 0) {
            mWaitDialog = ProgressDialog.show(this, null, "Cargando productos ...");
            mWaitDialog.setCancelable(true);
            ArrayList<ProductTag> productTags = new ArrayList<>();
            for (int i=0; i < mTagDataList.size() ; i++) {
                productTags.add(new ProductTag((String) mTagDataList.get(i), (String) mTagDataList.get(i)));
                System.out.println("Tag: " + mTagDataList.get(i));
            }
            for (ProductTag productTag: productTags) {
                viewModel.insert(productTag);
            }
            mTagDataList.clear();
            mAdapter.clear();
            Toast.makeText(ReadActivity.this, String.format("Productos registrados: %s", productTags.size()), Toast.LENGTH_SHORT).show();
            closeWaitDialog();
            finish();
        }
    }

/*    private void doSaveTags(RequestProductTag request) {
        WebServices.wsPostTags(request, this, new ResponseProductTagCallback() {
            @Override
            public void baseResponse(List<ProductTag> responseProductTag) {
                closeWaitDialog();
                for(ProductTag productTag: responseProductTag) {
                    viewModel.insert(productTag);
                }
                mTagDataList.clear();
                mAdapter.clear();
                Toast.makeText(ReadActivity.this, String.format("Productos registrados: %s", responseProductTag.size()), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void baseError(APIError productTagError) {
                closeWaitDialog();
                AlertDialog.Builder builder = new AlertDialog.Builder(ReadActivity.this);
                builder.setMessage(productTagError.message().toString()).setTitle("Error: " + String.valueOf(productTagError.status()));
                builder.setNegativeButton(R.string.close, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

    }*/

    private EventListener mEventListener = new EventListener() {
        @Override
        public void onDeviceConnected(Object o) {
        }

        @Override
        public void onDeviceDisconnected(Object o) {
        }

        @Override
        public void onReaderCreated(boolean b, RfidReader rfidReader) {
        }

        @Override
        public void onRfidTriggered(boolean trigger) {
            if (mIsReadBtnClicked || !trigger) {
                mIsReadBtnClicked = false;
                stopRead();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showBtn();
                    }
                });
            } else {
                mIsReadBtnClicked = true;
                read();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showBtn();
                    }
                });
            }
        }

        @Override
        public void onTriggerModeSwitched(TriggerMode triggerMode) {
        }
    };


    private boolean isReaderAvailable() {
        return mReader != null && mReader.available();
    }

    private void  read() {
        if (isReaderAvailable()) {
            mTagDataList.clear();
            mReader.setOnTagReadListener(dataListener);
            mReader.read(TagAdditionData.get("None"), new TagReadOption());
        } else {
            System.out.println(mReader);
        }
    }

    private void stopRead() {
        if (isReaderAvailable()) {
            mReader.stopRead();
            mReader.removeOnTagReadListener(dataListener);
        }
    }

    private OnTagReadListener dataListener = new OnTagReadListener() {
        @Override
        public void onTagRead(final TagReadData[] t) {
            synchronized (mTagDataList) {
                for (TagReadData trd : t) {
                    String epc = trd.getEpcHexStr();
                    if (!mTagDataList.contains(epc)) {
                        mTagDataList.add(epc);
                    }
                }
                mHandler.sendEmptyMessage(0);
            }
        }
    };

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    mAdapter.notifyDataSetChanged();
                    break;
            }
        }
    };

}