package com.reader.rfid.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.preference.PreferenceManager;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.chip.Chip;
import com.honeywell.aidc.BarcodeReader;
import com.honeywell.aidc.InvalidScannerNameException;
import com.honeywell.aidc.ScannerUnavailableException;
import com.honeywell.rfidservice.EventListener;
import com.honeywell.rfidservice.RfidManager;
import com.honeywell.rfidservice.TriggerMode;
import com.honeywell.rfidservice.rfid.RfidReader;
import com.reader.rfid.R;
import com.reader.rfid.adapters.MenuItemAdapter;
import com.reader.rfid.models.menu.MenuItem;

import com.honeywell.aidc.AidcManager;

import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {
    ImageButton logoutButton;
    TextView usernameText;
    TextView rolenameText;

    private GridView gridView;
    private MenuItemAdapter menuItemAdapter;

    // Barcoder reader
    private AidcManager manager;
    private Chip barcodeStatus;
    private Chip bluetoothStatus;
    private static BarcodeReader barcodeReader;

    // MyApp
    private App myApp;

    //Bluetooth
    private RfidManager mRfidMgr;
    public String macAddress;
    private BluetoothAdapter mBluetoothAdapter;
    private ProgressDialog mWaitDialog;
    private Handler mHandler = new Handler();
    private static final int PERMISSION_REQUEST_CODE = 1;
    private String[] mPermissions = new String[]{Manifest.permission.ACCESS_COARSE_LOCATION};
    private List<String> mRequestPermissions = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myApp = App.getInstance();
        mRfidMgr = myApp.rfidManager;
        setContentView(R.layout.activity_home);
        // Preferences
        requestPermissions();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        // Settings
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        Bundle extras = getIntent().getExtras();
        Resources res = this.getResources();
        logoutButton = (ImageButton) findViewById(R.id.logoutButton);
        usernameText = (TextView) findViewById(R.id.usernameTextView);
        rolenameText = (TextView) findViewById(R.id.rolenmeTextView);
        String usernameString;
        String rolenameString;
        if (extras != null) {
            usernameString = extras.getString(res.getString(R.string.userNameExtra));
            rolenameString = extras.getString(res.getString(R.string.roleExtra));
            usernameText.setText(usernameString);
            rolenameText.setText(rolenameString);
        }
        // Grid
        gridView = (GridView) findViewById(R.id.menuGrid);
        menuItemAdapter = new MenuItemAdapter(this);
        gridView.setAdapter(menuItemAdapter);
        gridView.setOnItemClickListener(this);
        barcodeStatus = (Chip) findViewById(R.id.barcodeStatusChip);
        barcodeStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (barcodeReader != null) {
                        barcodeReader.claim();
                        Toast.makeText(HomeActivity.this, R.string.barcodeReadyTitle, Toast.LENGTH_SHORT).show();
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
                        builder.setTitle(R.string.barcodeNotAvailableTitle);
                        builder.setMessage(R.string.barcodeNotAvailableBody);
                        builder.setNegativeButton(R.string.close, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                } catch (ScannerUnavailableException e) {
                    e.printStackTrace();
                }
            }
        });
        bluetoothStatus = (Chip) findViewById((R.id.bluetoothStatus));
        bluetoothStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBluetoothAdapter.isEnabled()) {
                    Intent intent = new Intent(HomeActivity.this, BluetoothActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(HomeActivity.this, R.string.bluetoothRequire, Toast.LENGTH_SHORT).show();
                }
            }
        });
        if (mBluetoothAdapter.isEnabled()) {
            bluetoothStatus.setChipStrokeColorResource(R.color.baseGreenStatus);
        } else {
            bluetoothStatus.setChipStrokeColorResource(R.color.baseRedStatus);
        }
        // Barcode reader
        // create the AidcManager providing a Context and a
        // CreatedCallback implementation.
        AidcManager.create(this, new AidcManager.CreatedCallback() {

            @Override
            public void onCreated(AidcManager aidcManager) {
                manager = aidcManager;
                try {
                    barcodeReader = manager.createBarcodeReader();
                    barcodeStatus.setChipBackgroundColorResource(R.color.baseGreenStatus);
                } catch (InvalidScannerNameException e) {
                    Toast.makeText(HomeActivity.this, "Invalid Scanner Name Exception: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    barcodeStatus.setChipBackgroundColorResource(R.color.baseRedStatus);
                } catch (Exception e) {
                    barcodeStatus.setChipBackgroundColorResource(R.color.baseRedStatus);
                    Toast.makeText(HomeActivity.this, "Exception: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
        // Bluetooth
        macAddress = myApp.getMacAddress();
        if (mBluetoothAdapter != null) {
            try {
                Boolean autoConnect = preferences.getBoolean("readerSettingsAutoConnect", true);
                if (mBluetoothAdapter.isEnabled() && macAddress != null && !macAddress.isEmpty() && autoConnect) {
                    connect();
                }
            } catch (Exception e) {
            }

        }
    }

    static BarcodeReader getBarcodeObject() {
        return barcodeReader;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (barcodeReader != null) {
            try {
                barcodeReader.claim();
            } catch (ScannerUnavailableException e) {
                e.printStackTrace();
                Toast.makeText(this, "Scanner unavailable", Toast.LENGTH_SHORT).show();
            }
        }
        mRfidMgr.addEventListener(managerEventListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mHandler.removeCallbacksAndMessages(null);
        mRfidMgr.removeEventListener(managerEventListener);
    }

    private void connect() {
        if (macAddress.isEmpty()) {
            return;
        }
        mRfidMgr.addEventListener(managerEventListener);
        mRfidMgr.connect(macAddress);
        mWaitDialog = ProgressDialog.show(this, null, getResources().getString(R.string.connDefDevice));
        mWaitDialog.setCancelable(true);

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                stopScan();
            }
        }, 5 * 1000);
    }

    // Permissions
    private boolean requestPermissions() {
        if (Build.VERSION.SDK_INT >= 23) {
            for (int i = 0; i < mPermissions.length; i++) {
                if (ContextCompat.checkSelfPermission(this, mPermissions[i]) != PackageManager.PERMISSION_GRANTED) {
                    mRequestPermissions.add(mPermissions[i]);
                }
            }

            if (mRequestPermissions.size() > 0) {
                ActivityCompat.requestPermissions(this, mPermissions, PERMISSION_REQUEST_CODE);
                return false;
            }
        }

        return true;
    }

    private void stopScan() {
        closeWaitDialog();
    }


    private void closeWaitDialog() {
        if (mWaitDialog != null) {
            mWaitDialog.dismiss();
            mWaitDialog = null;
        }
    }

    // Listeners
    private EventListener managerEventListener = new EventListener() {
        @Override
        public void onDeviceConnected(Object o) {
            myApp.macAddress = (String) o;

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    closeWaitDialog();
                    Toast.makeText(HomeActivity.this, getResources().getString(R.string.bluetooth_status, macAddress), Toast.LENGTH_SHORT)
                            .show();
                }
            });
        }

        @Override
        public void onDeviceDisconnected(Object o) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    closeWaitDialog();
                    Toast.makeText(HomeActivity.this, R.string.devDisconn, Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        public void onReaderCreated(boolean b, RfidReader rfidReader) {
            App.getInstance().mRfidManager = rfidReader;
        }

        @Override
        public void onRfidTriggered(boolean b) {
        }

        @Override
        public void onTriggerModeSwitched(TriggerMode triggerMode) {
        }
    };

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.closeHomeAlert);
        builder.setCancelable(true);
        builder.setPositiveButton(R.string.btnLogOut, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                signOutAlert();
            }
        });
        builder.setNegativeButton(R.string.btnCloseApp, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                closeAppAlert();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void signOutAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.signOutAlert);
        builder.setCancelable(true);
        builder.setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void closeAppAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.closeAppAlert);
        builder.setCancelable(true);
        builder.setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                startActivity(intent);
                int pid = android.os.Process.myPid();
                android.os.Process.killProcess(pid);
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void onClickLogoutButton(View view) {
        signOutAlert();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        MenuItem item = (MenuItem) parent.getItemAtPosition(position);
        Class activityToSend = item.getActivity();
        Intent intent = new Intent(this, activityToSend);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (barcodeReader != null) {
            // close BarcodeReader to clean up resources.
            barcodeReader.close();
            barcodeReader = null;
        }

        if (manager != null) {
            // close AidcManager to disconnect from the scanner service.
            // once closed, the object can no longer be used.
            manager.close();
        }
    }
}