package com.reader.rfid.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.honeywell.rfidservice.RfidManager;
import com.honeywell.rfidservice.EventListener;
import com.honeywell.rfidservice.TriggerMode;
import com.honeywell.rfidservice.rfid.RfidReader;

import com.reader.rfid.R;
import com.reader.rfid.models.bluetooth.BtDeviceInfo;

import java.util.ArrayList;
import java.util.List;

public class BluetoothActivity extends AppCompatActivity {

    Toolbar toolbar;

    private static final int PERMISSION_REQUEST_CODE = 1;
    private String[] mPermissions = new String[]{Manifest.permission.ACCESS_COARSE_LOCATION};
    private List<String> mRequestPermissions = new ArrayList<>();
    // MyApp
    private App myApp;
    // Bluetooth
    private BluetoothAdapter mBluetoothAdapter;
    // RFID
    private RfidManager mRfidMgr;
    // UI
    private Handler mHandler = new Handler();
    private ProgressDialog mWaitDialog;
    private TextView mTvInfo;
    private Button mBtnConnect;
    private ListView mLv;

    // ADAPTER
    private BtDeviceAdapter btDeviceAdapter;
    private List<BtDeviceInfo> mDevices = new ArrayList();
    private int mSelectedIdx = -1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myApp = App.getInstance();
        mRfidMgr = myApp.rfidManager;

        setContentView(R.layout.activity_bluetooth);
        // UI
        toolbar = findViewById(R.id.toolbarBt);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        toolbar.setTitle(R.string.btActivityTitle);
        toolbar.setSubtitle(R.string.btActivitySub);

        mTvInfo = findViewById(R.id.tv_info);
        mBtnConnect = findViewById(R.id.btn_connect);
        showBtn();
        mLv = findViewById(R.id.lv);

        // Adapter
        btDeviceAdapter = new BtDeviceAdapter(this, mDevices);
        mLv.setAdapter(btDeviceAdapter);
        mLv.setOnItemClickListener(btDeviceClickListener);

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        requestPermissions();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mRfidMgr.addEventListener(managerEventListener);
        scan();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mHandler.removeCallbacksAndMessages(null);
        mRfidMgr.removeEventListener(managerEventListener);
    }

    // MENU
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.bt_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else if(item.getItemId() == R.id.scan) {
            scan();
        }
        return super.onOptionsItemSelected(item);
    }

    // Permissions
    private boolean requestPermissions() {
        if (Build.VERSION.SDK_INT >= 23) {
            for (int i = 0; i < mPermissions.length; i++) {
                if (ContextCompat.checkSelfPermission(this, mPermissions[i]) != PackageManager.PERMISSION_GRANTED) {
                    mRequestPermissions.add(mPermissions[i]);
                }
            }

            if (mRequestPermissions.size() > 0) {
                ActivityCompat.requestPermissions(this, mPermissions, PERMISSION_REQUEST_CODE);
                return false;
            }
        }

        return true;
    }

    // RENDER METHODS
    private void showBtn() {
        mTvInfo.setTextColor(Color.rgb(128, 128, 128));

        if (isConnected()) {
            mTvInfo.setText(getResources().getString(R.string.bluetooth_status, myApp.macAddress));
            mTvInfo.setTextColor(Color.rgb(0, 128, 0));
            mBtnConnect.setEnabled(true);
            mBtnConnect.setText(R.string.disconnected);
        } else {
            mTvInfo.setText(R.string.deviceNotConn);
            mBtnConnect.setEnabled(mSelectedIdx != -1);
            mBtnConnect.setText(R.string.connect);
        }
    }

    private boolean isConnected() {
        return mRfidMgr.isConnected();
    }

    public void clickBtnConn(View v) {
        if (isConnected()) {
            disconnect();
        } else {
            connect();
        }
    }

    // SCAN
    private void scan() {
        if (!requestPermissions()) {
            return;
        }
        mDevices.clear();
        mSelectedIdx = -1;
        btDeviceAdapter.notifyDataSetChanged();
        mBluetoothAdapter.startLeScan(btLeScanCallback);

        mWaitDialog = ProgressDialog.show(this, null, getString(R.string.scanning));
        mWaitDialog.setCancelable(false);

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                stopScan();
            }
        }, 5 * 1000);
    }

    private void stopScan() {
        mBluetoothAdapter.stopLeScan(btLeScanCallback);
        closeWaitDialog();
    }

    private void connect() {
        if (mSelectedIdx == -1 || mSelectedIdx >= mDevices.size()) {
            return;
        }
        mRfidMgr.addEventListener(managerEventListener);
        String newMacAddress = mDevices.get(mSelectedIdx).dev.getAddress();
        mRfidMgr.connect(newMacAddress);
        App.getInstance().setMacAddress(newMacAddress);
        mWaitDialog = ProgressDialog.show(this, null, getResources().getString(R.string.connecting));
    }

    private void disconnect() {
        mRfidMgr.disconnect();
    }

    private void closeWaitDialog() {
        if (mWaitDialog != null) {
            mWaitDialog.dismiss();
            mWaitDialog = null;
        }
    }

    // Listeners
    private EventListener managerEventListener = new EventListener() {
        @Override
        public void onDeviceConnected(Object o) {
            myApp.macAddress = (String) o;

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showBtn();
                    closeWaitDialog();
                }
            });
        }

        @Override
        public void onDeviceDisconnected(Object o) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showBtn();
                    closeWaitDialog();
                }
            });
        }

        @Override
        public void onReaderCreated(boolean b, RfidReader rfidReader) {
            App.getInstance().mRfidManager = rfidReader;
        }

        @Override
        public void onRfidTriggered(boolean b) {
        }

        @Override
        public void onTriggerModeSwitched(TriggerMode triggerMode) {
        }
    };

    private long mPrevListUpdateTime;
    private BluetoothAdapter.LeScanCallback btLeScanCallback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
            if (device.getName() != null && !device.getName().isEmpty()) {
                synchronized (mDevices) {
                    boolean newDevice = true;

                    for (BtDeviceInfo info : mDevices) {
                        if (device.getAddress().equals(info.dev.getAddress())) {
                            newDevice = false;
                            info.rssi = rssi;
                        }
                    }

                    if (newDevice) {
                        mDevices.add(new BtDeviceInfo(device, rssi));
                    }

                    long cur = System.currentTimeMillis();

                    if (newDevice || cur - mPrevListUpdateTime > 500) {
                        mPrevListUpdateTime = cur;

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                btDeviceAdapter.notifyDataSetChanged();
                            }
                        });
                    }
                }
            }
        }
    };

    //ADAPTER
    private class BtDeviceAdapter extends ArrayAdapter {
        private Context ctx;

        public BtDeviceAdapter(Context context, List ls) {
            super(context, 0, ls);
            ctx = context;
        }

        public View getView(int position, @Nullable View v, @NonNull ViewGroup parent) {
            ViewHolder vh;

            if (v == null) {
                LayoutInflater inflater = LayoutInflater.from(ctx);
                v = inflater.inflate(R.layout.item_bt_device, null);
                vh = new ViewHolder();
                vh.tvName = v.findViewById(R.id.tvName);
                vh.tvAddr = v.findViewById(R.id.tvAddr);
                vh.tvRssi = v.findViewById(R.id.tvRssi);
                v.setTag(vh);
            } else {
                vh = (ViewHolder) v.getTag();
            }

            BtDeviceInfo item = mDevices.get(position);
            vh.tvName.setText(item.dev.getName());
            vh.tvAddr.setText(item.dev.getAddress());
            vh.tvRssi.setText(String.valueOf(item.rssi));

            if (position == mSelectedIdx) {
                v.setBackgroundColor(Color.rgb(220, 220, 220));
            } else {
                v.setBackgroundColor(Color.argb(0, 0, 0, 0));
            }

            return v;
        }

        class ViewHolder {
            TextView tvName;
            TextView tvAddr;
            TextView tvRssi;
        }
    }

    private AdapterView.OnItemClickListener btDeviceClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            mSelectedIdx = i;
            btDeviceAdapter.notifyDataSetChanged();
            showBtn();
        }
    };
}