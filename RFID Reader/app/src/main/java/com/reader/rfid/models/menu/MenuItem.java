package com.reader.rfid.models.menu;

import com.reader.rfid.R;
import com.reader.rfid.activities.FinishProductActivity;
import com.reader.rfid.activities.InventaryActivity;
import com.reader.rfid.activities.LoginActivity;

public class MenuItem {
    private int type;
    private Class activity;

    public MenuItem(int type, Class activity) {
        this.type = type;
        this.activity = activity;
    }

    public int getType() {
        return type;
    }

    public Class getActivity() {
        return activity;
    }

    public static MenuItem[] ITEMS = {
            new MenuItem(R.string.menuItemInventarySurvey, InventaryActivity.class),
/*
            new MenuItem(R.string.menuItemFinishedProduct, FinishProductActivity.class),
            new MenuItem(R.string.menuItemUpdateLocation, LoginActivity.class),
            new MenuItem(R.string.menuItemPicking, LoginActivity.class),
            new MenuItem(R.string.menuItemCageEntry, LoginActivity.class),
            new MenuItem(R.string.menuItemQualityReview, LoginActivity.class),
            new MenuItem(R.string.menuItemInventarySurvey, LoginActivity.class),*/
    };

    public static MenuItem getMenuItem(int typeId) {
        for (MenuItem item: ITEMS) {
            if(item.getType() == typeId) {
                return item;
            }
        }
        return null;
    };
}
