package com.reader.rfid.repository;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.reader.rfid.db.AppDatabase;
import com.reader.rfid.db.dao.ProductTagDAO;
import com.reader.rfid.models.ProductTag;

import java.util.List;

public class ProductTagRepository {

    private ProductTagDAO productTagDAO;
    private LiveData<List<ProductTag>> allTags;

    public ProductTagRepository(Application application) {
        AppDatabase db = AppDatabase.getInstance(application);
        productTagDAO = db.productTadDao();
        allTags = productTagDAO.getAll();
    }

    public void insert (ProductTag productTag) {
        new InsertProductTagAsyncTask(productTagDAO).execute(productTag);
    }

    public void delete (ProductTag productTag) {
        new DeleteProductTagAsyncTask(productTagDAO).execute(productTag);
    }

    public void deleteAll () {
        new DeleteAllProductTagAsyncTask(productTagDAO).execute();
    }

    public LiveData<List<ProductTag>> getAllTags() {
        return allTags;
    }

    private static class InsertProductTagAsyncTask extends AsyncTask<ProductTag, Void, Void> {

        private ProductTagDAO productTagDAO;

        public InsertProductTagAsyncTask(ProductTagDAO productTagDAO) {
            this.productTagDAO = productTagDAO;
        }

        @Override
        protected Void doInBackground(ProductTag... productTags) {
            productTagDAO.insert(productTags[0]);
            return null;
        }
    }

    private static class DeleteProductTagAsyncTask extends AsyncTask<ProductTag, Void, Void> {

        private ProductTagDAO productTagDAO;

        public DeleteProductTagAsyncTask(ProductTagDAO productTagDAO) {
            this.productTagDAO = productTagDAO;
        }

        @Override
        protected Void doInBackground(ProductTag... productTags) {
            productTagDAO.delete(productTags[0]);
            return null;
        }
    }

    private static class DeleteAllProductTagAsyncTask extends AsyncTask<Void, Void, Void> {

        private ProductTagDAO productTagDAO;

        public DeleteAllProductTagAsyncTask(ProductTagDAO productTagDAO) {
            this.productTagDAO = productTagDAO;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            productTagDAO.deleteAll();
            return null;
        }
    }

}
