package com.reader.rfid.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.reader.rfid.R;
import com.reader.rfid.models.menu.MenuItem;

/**
 * {@link BaseAdapter} Creates menus items in a grid view
 */

public class MenuItemAdapter extends BaseAdapter {
    private Context context;

    public MenuItemAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return MenuItem.ITEMS.length;
    }

    @Override
    public MenuItem getItem(int position) {
        return MenuItem.ITEMS[position];
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getType();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_menu, parent, false);
        }
        TextView menuItemText = (TextView) convertView.findViewById(R.id.menuItemText);
        final MenuItem item = getItem(position);
        Resources res = context.getResources();
        menuItemText.setText(res.getText(item.getType()));

        return convertView;
    };
}
