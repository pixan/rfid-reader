package com.reader.rfid.db;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.reader.rfid.db.dao.ProductTagDAO;
import com.reader.rfid.db.dao.WarehouseDAO;
import com.reader.rfid.models.ProductTag;
import com.reader.rfid.models.warehouse.Warehouse;

import org.jetbrains.annotations.NotNull;

@Database(entities = {ProductTag.class, Warehouse.class}, version = 3)
public abstract class AppDatabase extends RoomDatabase {
    // DAO
    public abstract ProductTagDAO productTadDao();
    public abstract WarehouseDAO warehouseDao();
    //Instance
    private static AppDatabase instance;
    public static synchronized AppDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(
                    context.getApplicationContext(),
                    AppDatabase.class,
                    "rfid_db")
                    .fallbackToDestructiveMigration()
                    .addCallback(roomCallback)
                    .build();
        }
        return instance;
    }
    //Populate
    private static final RoomDatabase.Callback roomCallback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull @NotNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new PopulateDbAsyncTask(instance).execute();
        }
    };

    private static class PopulateDbAsyncTask extends AsyncTask<Void, Void, Void> {
        private final WarehouseDAO warehouseDao;
        private PopulateDbAsyncTask(AppDatabase db) {
            this.warehouseDao = db.warehouseDao();
        }
        @Override
        protected Void doInBackground(Void... voids) {
            warehouseDao.insertWarehouse(new Warehouse(1, "Principal"));
            warehouseDao.insertWarehouse(new Warehouse(2, "ZMG"));
            warehouseDao.insertWarehouse(new Warehouse(3, "Bajio"));
            return null;
        }
    }
}
