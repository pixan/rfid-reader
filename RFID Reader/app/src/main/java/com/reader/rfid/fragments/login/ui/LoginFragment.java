package com.reader.rfid.fragments.login.ui;

import static android.content.Context.INPUT_METHOD_SERVICE;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.progressindicator.LinearProgressIndicator;
import com.reader.rfid.R;
import com.reader.rfid.activities.HomeActivity;
import com.reader.rfid.databinding.FragmentLoginBinding;
import com.reader.rfid.models.user.UserModel;

import java.util.regex.Pattern;

public class LoginFragment extends Fragment {

    private LoginViewModel loginViewModel;
    private FragmentLoginBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = FragmentLoginBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loginViewModel = new ViewModelProvider(this, new LoginViewModelFactory())
                .get(LoginViewModel.class);
        final EditText usernameEditText = binding.username;
        final EditText passwordEditText = binding.password;
        final Button loginButton = binding.login;
        final LinearProgressIndicator loadingProgressBar = binding.loading;

        loginViewModel.getLoginFormState().observe(getViewLifecycleOwner(), new Observer<LoginFormState>() {
            @Override
            public void onChanged(@Nullable LoginFormState loginFormState) {
                if (loginFormState == null) {
                    return;
                }
                loginButton.setEnabled(loginFormState.isDataValid());
                if (loginFormState.getUsernameError() != null) {
                    usernameEditText.setError(getString(loginFormState.getUsernameError()));
                }
                if (loginFormState.getPasswordError() != null) {
                    passwordEditText.setError(getString(loginFormState.getPasswordError()));
                }
            }
        });

        TextWatcher afterTextChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                loginViewModel.loginDataChanged(usernameEditText.getText().toString(),
                        passwordEditText.getText().toString());
            }
        };
        usernameEditText.addTextChangedListener(afterTextChangedListener);
        passwordEditText.addTextChangedListener(afterTextChangedListener);
        passwordEditText.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                loadingProgressBar.setVisibility(View.VISIBLE);
                usernameEditText.setEnabled(false);
                passwordEditText.setEnabled(false);
                loginButton.setEnabled(false);
                hideSoftKeyboard(requireActivity());
                doLogin(usernameEditText.getText().toString(), passwordEditText.getText().toString(), loadingProgressBar, usernameEditText, passwordEditText, loginButton);
            }
            return false;
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadingProgressBar.setVisibility(View.VISIBLE);
                usernameEditText.setEnabled(false);
                passwordEditText.setEnabled(false);
                loginButton.setEnabled(false);
                hideSoftKeyboard(requireActivity());
                doLogin(usernameEditText.getText().toString(), passwordEditText.getText().toString(), loadingProgressBar, usernameEditText, passwordEditText, loginButton);
            }
        });
    }

    public static void hideSoftKeyboard(Activity activity) {
        if (activity.getCurrentFocus() == null) {
            return;
        }
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    private void doLogin(String username, String password, LinearProgressIndicator loadingProgressBar, EditText usernameEditText, EditText passwordEditText, Button loginButton) {
        // Request
       String userName1 = username;
        // TODO: Remove once we got the ws
        String regex = "^\\d+$";
        if (Pattern.compile(regex).matcher(username).matches()) {
            if (username.equals("112233")) {
                userName1 = "jcarlo@bwdigital.io";
            }
        }
        if(userName1.equals("jcarlo@bwdigital.io") && password.equals("Secret123")) {
            if (getContext() != null && getContext().getApplicationContext() != null) {
                // TODO: Modify flags
                Intent homeIntent = new Intent(getActivity(), HomeActivity.class);
                UserModel user = new UserModel("1234567890", "Jose Medina", "Administrador");
                Resources res = getContext().getResources();
                homeIntent.putExtra(String.valueOf(res.getString(R.string.userNameExtra)), user.getUsername().toString());
                homeIntent.putExtra(String.valueOf(res.getString(R.string.tokenExtra)), user.getToken().toString());
                homeIntent.putExtra(String.valueOf(res.getString(R.string.roleExtra)), user.getRoleName().toString());
                startActivity(homeIntent);
            }
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("Usuario/contraseña incorrecta").setTitle("Error de autenticación");
            builder.setNegativeButton(R.string.close, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
            loadingProgressBar.setVisibility(View.GONE);
            usernameEditText.setEnabled(true);
            passwordEditText.setEnabled(true);
            loginButton.setEnabled(true);
        }
/*        LoginRequest loginRequest = new LoginRequest(user, password);
        WebServices.wsPostLogin(loginRequest, getContext(), new ResponseCallback() {
            @Override
            public void baseResponse(ResponseBody response) {
                loadingProgressBar.setVisibility(View.GONE);
                usernameEditText.setText("");
                passwordEditText.setText("");
                usernameEditText.setEnabled(true);
                passwordEditText.setEnabled(true);
                loginButton.setEnabled(true);
                if (getContext() != null && getContext().getApplicationContext() != null) {
                    // TODO: Modify flags
                    Intent homeIntent = new Intent(getActivity(), HomeActivity.class);
                    UserModel user = new UserModel("1234567890", "John Dou", "Almacenista");
                    Resources res = getContext().getResources();
                    homeIntent.putExtra(String.valueOf(res.getString(R.string.userNameExtra)), user.getUsername().toString());
                    homeIntent.putExtra(String.valueOf(res.getString(R.string.tokenExtra)), user.getToken().toString());
                    homeIntent.putExtra(String.valueOf(res.getString(R.string.roleExtra)), user.getRoleName().toString());
                    startActivity(homeIntent);
                }
            }

            @Override
            public void baseError(APIError loginError) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(loginError.message().toString()).setTitle("Error: " + String.valueOf(loginError.status()));
                builder.setNegativeButton(R.string.close, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
                loadingProgressBar.setVisibility(View.GONE);
                usernameEditText.setEnabled(true);
                passwordEditText.setEnabled(true);
                loginButton.setEnabled(true);
            }
        });*/
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}