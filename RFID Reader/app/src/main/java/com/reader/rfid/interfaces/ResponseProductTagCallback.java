package com.reader.rfid.interfaces;

import com.reader.rfid.models.APIError;
import com.reader.rfid.models.ProductTag;

import java.util.List;

public interface ResponseProductTagCallback {
    void baseResponse(List<ProductTag> responseProductTag);
    void baseError(APIError productTagError);
}
