package com.reader.rfid.utils;

import android.content.Context;
import android.content.res.Resources;

import com.reader.rfid.R;
import com.reader.rfid.models.APIError;
import com.reader.rfid.models.response.ResponseBody;
import com.reader.rfid.models.response.ResponseProductTag;

import retrofit2.Response;

public class ErrorUtils {
    public APIError getLoginErrors(Response<ResponseBody> response, Context context) {
        Resources res = context.getResources();
        switch (response.code()) {
            case 401:
                return new APIError(response.code(), res.getString(R.string.loginError401));
            default:
                return new APIError(response.code(), res.getString(R.string.requestFailed));
        }
    };
    public APIError getProductTagsErrors(Response<ResponseProductTag> response, Context context) {
        Resources res = context.getResources();
        switch (response.code()) {
            default:
                return new APIError(response.code(), response.message());
        }
    };
}
