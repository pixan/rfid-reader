package com.reader.rfid.models.login;

public class LoginRequest {
    final String user;
    final String secret;
    final String provider = "email";
    public LoginRequest(String user, String secret) {
        this.user = user;
        this.secret = secret;
    }
}
