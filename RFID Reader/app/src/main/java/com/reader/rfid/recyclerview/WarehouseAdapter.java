package com.reader.rfid.recyclerview;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.reader.rfid.R;
import com.reader.rfid.models.warehouse.Warehouse;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;


public class WarehouseAdapter extends RecyclerView.Adapter<WarehouseAdapter.WarehouseHolder> {

    private List<Warehouse> warehouses = new ArrayList<>();

    @Override
    public WarehouseHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_dropdown_warehouse, parent, false);
        return new WarehouseHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull WarehouseAdapter.WarehouseHolder holder, int position) {
        Warehouse warehouse = warehouses.get(position);
        holder.name.setText(warehouse.getName() == null ? "Almacén no encontrado" : warehouse.getName());
    }

    @Override
    public int getItemCount() {
        return warehouses.size();
    }

    public void setWarehouses(List<Warehouse> warehouses) {
        this.warehouses = warehouses;
        notifyDataSetChanged();
    }

    public Warehouse getWarehouseAt(int position) {
        return warehouses.get(position);
    }

    class WarehouseHolder extends RecyclerView.ViewHolder {
        private TextView name;

        public WarehouseHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.titleTag);
        }
    }

}
