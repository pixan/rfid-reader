package com.reader.rfid.models.response;

import java.util.Optional;

public class ResponseBody {
    private String status;
    private Optional<Object> messages;
    private Optional<Object> data;

    public ResponseBody(String status, Optional<Object> messages, Optional<Object> data) {
        this.status = status;
        this.messages = messages;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Optional<Object> getMessages() {
        return messages;
    }

    public void setMessages(Optional<Object> messages) {
        this.messages = messages;
    }

    public Optional<Object> getData() {
        return data;
    }

    public void setData(Optional<Object> data) {
        this.data = data;
    }
}
