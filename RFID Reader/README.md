# Therapedic RFID
Application aimed at operational environment to perform processes of entry to finished products, relocation of products, picking, processing to cages, quality management and inventory management.

## Installation
Clone this repository and import into **Android Studio**
```bash
git clone git@github.com:test/<reponame>.git
```

## Configuration
### Keystores:
Create `app/keystore.gradle` with the following info:
```gradle
ext.key_alias='...'
ext.key_password='...'
ext.store_password='...'
```
And place both keystores under `app/keystores/` directory:
- `development.keystore`


## Build variants
Use the Android Studio *Build Variants* button to choose between **production** and **staging** flavors combined with debug and release build types

## File architecture

## WS Integration

## Hardware - Honeywell

## Local storage - SQLite

## Generating signed APK
From Android Studio:
1. ***Build*** menu
2. ***Generate Signed APK...***
3. Fill in the keystore information *(you only need to do this once manually and then let Android Studio remember it)*

## Deploy

## Maintainers
This project is mantained by:
* [Raúl Reza](rreza@zigatta.com)
